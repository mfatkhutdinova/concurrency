build:
	mkdir -p bin
	go build -o bin/service_concurrency cmd/service_concurrency/main.go

run:
	go run cmd/service_concurrency/main.go
