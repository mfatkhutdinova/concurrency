package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

type Node struct {
	Parent *Node
	Childs []*Node
	Value  int
}

func (n *Node) Insert(value int) {
	n.Parent = nil
	n.Childs = []*Node{}
	n.Value = value
}

func (n *Node) PrintService() {
	fmt.Println("Start service ->", n.Value)
}

func (n *Node) AddChildServices(addrChild *Node) {
	n.Childs = append(n.Childs, addrChild)
}

func (n *Node) AddParentServices(addrParent *Node) {
	n.Parent = addrParent
}

func checkServices(n *Node, serviceNumber int) *Node {
	for _, child := range n.Childs {
		if child.Value == serviceNumber {
			return child
		} else {
			childValue := checkServices(child, serviceNumber)
			if childValue == nil {
				continue
			} else {
				return childValue
			}
		}
	}
	return nil
}

func collectParentServices(n *Node, parentServicesList []*Node) []*Node {
	parentServicesList = append(parentServicesList, n)
	if n.Parent != nil {
		return collectParentServices(n.Parent, parentServicesList)
	}

	return parentServicesList
}

func convertStringToNumber(fields []string, index int) int {
	serviceNumber, err := strconv.Atoi(fields[index])
	if err != nil {
		panic("Вы ввели некорректное значение.")
	}
	return serviceNumber
}

func startService(parentServices []*Node) {
	for i := len(parentServices) - 1; i >= 0; i-- {
		n := parentServices[i]
		n.PrintService()
	}
	fmt.Println("------Stop------")
}

func initializeServices(serviceRoot *Node) {
	fmt.Println("Набирайте номера сервисов через пробел. Первое число - номер родительского сервиса. Второе число - номер дочернего сервиса. " +
		"Для выхода нажмите Enter.")

	input := bufio.NewScanner(os.Stdin)
	for input.Scan() {
		line := input.Text()
		if len(line) == 0 {
			break
		}
		var serviceChild Node

		fields := strings.Fields(line)
		parentNumber := convertStringToNumber(fields, 0)
		childNumber := convertStringToNumber(fields, 1)

		if parentNumber == 0 {
			serviceChild.Insert(childNumber)
			serviceChild.AddParentServices(serviceRoot)
			serviceRoot.AddChildServices(&serviceChild)
		} else {
			serviceParent := checkServices(serviceRoot, parentNumber)
			serviceChildCheck := checkServices(serviceRoot, childNumber)
			if serviceParent == nil {
				fmt.Println("Ваш родительский сервис не определен. Добавьте для начала его в дочерние сервисы в имеющиеся сервисы или добавьте в root(0)")
				continue
			} else if serviceChildCheck != nil {
				fmt.Println("Ваш дочерний сервис уже определен.")
				continue
			} else {
				serviceChild.Insert(childNumber)
				serviceChild.AddParentServices(serviceParent)
				serviceParent.AddChildServices(&serviceChild)
			}
		}
	}
}


func main() {
	var serviceRoot Node
	serviceRoot.Insert(0)

	initializeServices(&serviceRoot)

	fmt.Println("Напишите через пробел какие сервисы нужно запустить: ")

	input := bufio.NewScanner(os.Stdin)
	for input.Scan() {
		line := input.Text()
		if len(line) == 0 {
			break
		}

		fmt.Println("Напишите время выполнения работы в миллисекундах для одного сервиса: ")
		var timer float64
		fmt.Scan(&timer)

		startFields := strings.Fields(line)
		for i := range startFields {
			var parentServicesList []*Node

			start := time.Now()

			startServices := convertStringToNumber(startFields, i)
			findStartServices := checkServices(&serviceRoot, startServices)
			if findStartServices == nil {
				fmt.Printf("Не могу запустить сервис - %v, т.к. его не существует.", startServices)
				continue
			} else {
				parentServicesList = collectParentServices(findStartServices, parentServicesList)
			}
			startService(parentServicesList)

			stop := time.Now()
			diff := stop.Sub(start)
			actualMs := diff.Seconds() * 1000
			ok := actualMs < timer
			if !ok {
				fmt.Printf("Вызов сервиса %v отработало за большее время, чем заданное. \n", startServices)
				panic("Завершение программы.")
			}
		}
	}
}
